# Stage 1: Build
FROM node:14-alpine AS Build
WORKDIR /usr/share/angular
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build

# Stage 2: 
FROM nginx:1.20-alpine
WORKDIR /etc/nginx
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=Build /usr/share/angular/dist/angular-demo /usr/share/nginx/html
